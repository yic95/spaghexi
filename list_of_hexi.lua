ListOfHexi = {  ---@type PCHexi[]
  {
    {
      rotation_tests = {{1, 0}, {-1, 0}, {2, 0}, {-2, 0}},
      shape = {{false, false, false, false, false},
                {false, false, false, false, false},
                {true,  true,  true,  true,  true }},
      center_col = 3,
      lowest_position = {5, 3},
      edge = {1, 3}
    },
    {
      rotation_tests = {{0, -1}, {0, 1}, {0, -2}, {0, 2},},
      shape = {{false, false, true},
                {false, false, true},
                {false, false, true},
                {false, false, true},
                {false, false, true}},
      center_col = 3,
      lowest_position = {3, 5},
      edge = {3, 1}
    },
    {
      rotation_tests = {},
      shape = {{false, false, false, false, true},
                {false, false, false, true , false},
                {false, false, true,  false, false},
                {false, true,  false, false, false},
                {true,  false, false, false, false}},
      center_col = 3,
      lowest_position = {1, 5},
      edge = {1, 1}
    },
    {
      rotation_tests = {{1, 0}, {-1, 0}, {2, 0}, {-2, 0}},
      shape = {{false, false, false, false, false},
                {false, false, false, false, false},
                {true,  true,  true,  true,  true }},
      center_col = 3,
      lowest_position = {5, 3},
      edge = {1, 3}
    },
    {
      rotation_tests = {{0, -1}, {0, 1}, {0, -2}, {0, 2},},
      shape = {{false, false, true},
                {false, false, true},
                {false, false, true},
                {false, false, true},
                {false, false, true}},
      center_col = 3,
      lowest_position = {3, 5},
      edge = {3, 1}
    },
    {
      rotation_tests = {},
      shape = {{false, false, false, false, true},
                {false, false, false, true , false},
                {false, false, true,  false, false},
                {false, true,  false, false, false},
                {true,  false, false, false, false}},
      center_col = 3,
      lowest_position = {1, 5},
      edge = {1, 1}
    }
  }
}
