
require("game")

local sq3ov2 = math.sqrt(3) / 2
local coord_top = {}
local coord_ceil = {}
local coord_bot = {}
local game_over = false

---@param x number `one-indexed`
---@param y number `one-indexed`
---@param scale number
---@param originX number
---@param originY number
---@return number, number
function to_screen_cord(x, y, scale, originX, originY)
  x = (x - 1) * scale
  y = (y - 1) * scale

  return x * sq3ov2 + originX, y + x / 2 + originY
end

function draw_hexigon(x, y, lineLen, mode)
  love.graphics.polygon(
    mode,
    x - lineLen, y,
    x - lineLen / 2, y + lineLen * sq3ov2,
    x + lineLen / 2, y + lineLen * sq3ov2,
    x + lineLen, y,
    x + lineLen / 2, y - lineLen * sq3ov2,
    x - lineLen / 2, y - lineLen * sq3ov2
  )
end

function love.load()
  setup({10, 40})
  coord_top, coord_ceil, coord_bot = get_matrix_info()
end

function love.update(dt)
  if game_over or tick(dt, 0, false, false, 0) == TickStatus.game_over then
    game_over = true
  end
end

function love.draw()
  for i, row in ipairs(get_matrix()) do
    for j, col in ipairs(row) do
      if i >= coord_ceil[j] and i <= coord_bot[j] then
        local coordx, coordy = to_screen_cord(j, i, 20, 10, -300)
        if col == 0 then
          draw_hexigon(coordx, coordy, 10, 'line')
        else
          draw_hexigon(coordx, coordy, 10, 'fill')
        end
      end
    end
  end
  local sms, pc = get_hexies()
  for _, h in pairs(sms) do
    if h.exists then
      for i, row in ipairs(h.shape) do
        for j, col in ipairs(row) do
          if col ~= 0 then
            local coordx, coordy = to_screen_cord(j + h.position[1] - 1, i + h.position[2] - 1, 20, 10, -300)
            draw_hexigon(coordx, coordy, 10, 'fill')
          end
        end
      end
    end
  end
  for i, row in ipairs(pc.shape) do
    for j, col in ipairs(row) do
      if col then
        local coordx, coordy = to_screen_cord(j + pc.position[1] - 1, i + pc.position[2] - 1, 20, 10, -300)
        draw_hexigon(coordx, coordy, 10, 'fill')
      end
    end
  end
  if game_over then
    love.graphics.rectangle('fill', 300, 100, 10, 10)
  end
end
