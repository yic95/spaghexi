require('list_of_hexi')

---@alias vec2 {[1]: integer, [2]: integer}
---@alias hvec2 {[1]: integer, [2]: number}
---@class PCHexiFace
---@field rotation_tests hvec2[] List of offsets used in rotation
---@field shape boolean[][] The shape of this particular orientation.
---@field center_col integer The center column of the shape
---@field lowest_position vec2 The lowest position the shape
---@field edge vec2 the left and top edge of the shape

---@alias PCHexi {[1]: PCHexiFace, [2]: PCHexiFace, [3]: PCHexiFace, [4]: PCHexiFace, [5]: PCHexiFace, [6]: PCHexiFace, }

---@class SMHexi
---@field exists boolean
---@field shape integer[][]
---@field position hvec2
---@field has_moved boolean Whether this Hexi has moved in this gravity tick

---@enum TickStatus
TickStatus = {
  next_hexi_wait = -1,
  soft_drop = 1,
  hard_drop_lock = 2,
  hard_drop_sm = 3,
  gravity_tick = 4,
  collapse = 5,
  game_over_spawn_fail = 6,
  game_over_over_ceiling = 7,
  gravity_tick_wait = 0
}

--- semi-global variables
-- the board
local g_matrix = {} ---@type integer[][] bitmap showing hiexes on the matrix
local g_board_top = 0  -- id of top layer of the matrix
local g_board_ceil = 0 -- id of top visible layer
local g_board_bot = 0  -- id of bottom layer
local g_sm_hexies = {} ---@type SMHexi[] list of self-moving hexies, exist or not
local g_pc_hexi = {} ---@type PCHexi the type of the pc hexi
local g_pc_hexi_pos = {} ---@type hvec2 the position of the pc hexi
local g_pc_hexi_orientation = 1 ---@type 1|2|3|4|5|6 the orientation of the pc hexi

-- the hexi generater
local g_hexi_gen_bag = {} ---@type integer[] list of indexes to list_of_hexi. shuffeled when all hexies have been generated
for i = 1, #ListOfHexi do
  g_hexi_gen_bag[#g_hexi_gen_bag + 1] = i
end
local g_hexi_gen_progress = 1 ---@type integer index to g_hexi_gen_bag showing what is the next index. reset to 1 when greater than #g_hexi_gen_bag

--- flags
local f_do_lock = false -- Lock PC Hexi after grav. tick
local f_do_collapse = false

--- timer caps (in seconds)
local c_gravity_tick_cap = 0.5
local c_pc_hexi_lock_cap = 1.0                    -- stablize delay cap.
local c_next_hexi_wait_cap = 0.7                  -- delay needed to wait before droping next hexi
local c_collapse_wait_cap = 0.4                   -- extra delay needed to wait after collapsing
local c_soft_drop_cap = c_gravity_tick_cap * 0.05 -- soft drop delay

--- timers
local c_pc_hexi_lock = 0
local c_gravity_tick = 0
local c_next_hexi_wait = c_next_hexi_wait_cap - 0.001 -- automatically activates when < cap
local c_collapse_wait = c_collapse_wait_cap + 1       -- automatically activates when < cap
local c_soft_drop = 0

g_sm_hexies = {
  {
    exists = true,
    has_moved = false,
    shape = { { 1, 1, 1, 1, 1 } },
    position = { 1, 23 }
  },
  {
    exists = true,
    has_moved = false,
    shape = { { 1, 1, 1, 1, 1 } },
    position = { 1, 24 }
  },
}

--- memos
-- quick lookup for space occupied by sm hexies
---@type (0|1|2|3)[][]
local m_sm_hexi_map = {}

local m_layer_lookup = {} ---@type integer[][]

local function shuf_array(arr)
  math.randomseed(os.clock())
  for i = #arr, 2, -1 do
    local x = math.random(1, i)
    if i ~= x then
      local tmp = arr[i]
      arr[i] = arr[x]
      arr[x] = tmp
    end
  end
end

---Return coordinates in the nth layer
---@param n integer
---@param matrix_width integer
---@return integer[]
local function nth_layer(n, matrix_width)
  if m_layer_lookup[n] == nil then
    local result = {} ---@type integer[]
    for i = 1, math.min(matrix_width, 2 * n) do
      result[#result + 1] = n - math.ceil(i / 2) + 1
    end
    m_layer_lookup[n] = result
  end
  return m_layer_lookup[n]
end

---@param bmp (boolean|integer)[][] The shape to be checked
---@param pos_x integer X position of the shape
---@param pos_y number Y position of the shape
---@param matrix integer[][] 2-D array showing what cells on matrix are filled with (immoveable) hiexes
---@param sm_hexi_map (0|1|2|3)[][]? 2-D array showing what hals-cells on matrix are filled with sm hexi
---@return boolean
local function has_no_collision(bmp, pos_x, pos_y, matrix, sm_hexi_map)
  for y, row in ipairs(bmp) do
    for x, piece in ipairs(row) do
      -- is cell filled?
      if not piece or piece == 0 then
        goto continue
      end

      local real_y = y + pos_y - 1
      local real_x = x + pos_x - 1

      -- boundary check
      if real_y < 1 or math.ceil(real_y) > #matrix then
        return false
      end
      if real_x < 1 or real_x > #matrix[math.ceil(real_y)] or real_x > #matrix[math.floor(real_y)] then
        return false
      end

      if pos_y % 1 == 0 then
        -- check collision with matrix
        if matrix[real_y][real_x] ~= 0 then
          return false
        end

        -- check collision with sm_hexi_map
        if sm_hexi_map then
          if sm_hexi_map[real_y][real_x] ~= 0 then
            return false
          end
        end
      else -- position not aligned
        -- check collision with matrix
        if matrix[y + math.ceil(pos_y) - 1][real_x] ~= 0 or
            matrix[y + math.floor(pos_y) - 1][real_x] ~= 0 then
          return false
        end

        -- check collision with sm_hexi_map
        -- checking upper half of the hiex
        if matrix[math.floor(real_y)][real_x] ~= 0 or matrix[math.ceil(real_y)][real_x] ~= 0 or
            sm_hexi_map and (sm_hexi_map[math.floor(real_y)][real_x] > 1 or sm_hexi_map[math.ceil(real_y)][real_x] % 2 == 1) then
          return false
        end
      end
      ::continue::
    end
  end
  return true
end

---Return new position of PC Hexi.
---@param hexi_face PCHexiFace
---@param spawn_layer integer
---@param spawn_column integer
---@return vec2
local function get_pc_hexi_spawn_position(hexi_face, spawn_column, spawn_layer, matrix_width)
  local pos_x = spawn_column - hexi_face.center_col + 1 -- one-indexed
  local pos_y = nth_layer(spawn_layer, matrix_width)[pos_x + hexi_face.lowest_position[1] - 1] -
      hexi_face.lowest_position[2] + 1
  return { pos_x, pos_y }
end

---Add bmp to matrix at position (pos_x, pos_y), rounded up.
---Return True if all cells are added.
---@param bmp boolean[][]
---@param pos_x integer
---@param pos_y number
---@param id integer
---@param matrix integer[][]
local function lock_to_matrix(bmp, pos_x, pos_y, id, matrix)
  pos_y = math.ceil(pos_y)
  for i, row in ipairs(bmp) do
    for j, cell in ipairs(row) do
      if not cell then
        goto continue
      end
      if pos_y + i - 1 < 1 or pos_y + i - 1 > #matrix or matrix[pos_y + i - 1][pos_x + j - 1] ~= 0 then -- nil is out of bound
        goto continue
      end
      matrix[pos_y + i - 1][pos_x + j - 1] = id
      ::continue::
    end
  end
end

---Add SM Hexi with provided info into `list` by mutating it. Return index to the added SM Hexi
---@param bmp integer[][]
---@param pos_x integer
---@param pos_y number
---@param list SMHexi[]
local function add_sm_hexi(bmp, pos_x, pos_y, list)
  for i, hexi in ipairs(list) do -- reusing old entries to save space
    if not hexi.exists then
      hexi.shape = bmp
      hexi.position[1] = pos_x
      hexi.position[2] = pos_y
      hexi.exists = true
      hexi.has_moved = false
      return i
    end
  end
  local newobj = { ---@type SMHexi
    shape = bmp,
    exists = true,
    has_moved = false,
    position = {}
  }
  newobj.position[1] = pos_x
  newobj.position[2] = pos_y
  list[#list + 1] = newobj
  return #list
end

---Remove empty rows and columns in shape and adjust position accordingly
---@return boolean[][] shp2
---@return vec2 pos2
local function strip_white_lines(shp, pos, edge)
  local trimmed_shape = {}
  local adj_pos = {}
  for i = edge[2], #shp do
    trimmed_shape[i - edge[2] + 1] = {}
    for j = edge[1], #shp[i] do
      trimmed_shape[i - edge[2] + 1][j - edge[1] + 1] = shp[i][j]
    end
  end
  adj_pos[1] = pos[1] + edge[1] - 1
  adj_pos[2] = pos[2] + edge[2] - 1
  return trimmed_shape, adj_pos
end

---Add `shp` to `map`.
---@param map (0|1|2|3)[][]
---@param shp boolean[][]|integer[][]
---@param pos hvec2
local function add_to_map(map, shp, pos)
  for y, row in ipairs(shp) do
    for x, cell in ipairs(row) do
      if cell and cell ~= 0 then
        if map[y + math.ceil(pos[2]) - 1][x + pos[1] - 1] % 2 == 0 then -- upper hiex on matrix
          map[y + math.ceil(pos[2]) - 1][x + pos[1] - 1] = map[y + math.ceil(pos[2]) - 1][x + pos[1] - 1] + 1
        end
        -- lower hiex
        map[y + math.floor(pos[2]) - 1][x + pos[1] - 1] = math.min(3, map[y + math.floor(pos[2]) - 1][x + pos[1] - 1] + 2)
      end
    end
  end
end

---cut `shp` from `map`
---@param map (0|1|2|3)[][]
---@param shp boolean[][]|integer[][]
---@param pos hvec2
local function cut_from_map(map, shp, pos)
  for y, row in ipairs(shp) do
    for x, cell in ipairs(row) do
      if cell and cell ~= 0 then
        if map[y + math.ceil(pos[2]) - 1][x + pos[1] - 1] % 2 ~= 0 then -- upper hiex on matrix
          map[y + math.ceil(pos[2]) - 1][x + pos[1] - 1] = map[y + math.ceil(pos[2]) - 1][x + pos[1] - 1] - 1
        end
        -- lower hiex
        if map[y + math.floor(pos[2]) - 1][x + pos[1] - 1] >= 2 then
          map[y + math.floor(pos[2]) - 1][x + pos[1] - 1] = math.min(3,
            map[y + math.floor(pos[2]) - 1][x + pos[1] - 1] - 2)
        end
      end
    end
  end
end

---update the map
---@param map (0|1|2|3)[][] the map to be updated
---@param sm_hexies SMHexi[] a list of sm hexies to be put on the map
---@param pc_hexi_shp (integer|boolean)[][]? the shape of pc hexi to be put on the map
---@param pc_hexi_pos hvec2? the position of the shape of the pc hexi
---@param no_reset boolean? don't reset the map before updating
local function update_hexi_map(map, sm_hexies, pc_hexi_shp, pc_hexi_pos, no_reset)
  if not no_reset then
    for i = 1, #map do
      for j = 1, #map[i] do
        map[i][j] = 0
      end
    end
  end
  for _, hexi in pairs(sm_hexies) do
    if hexi.exists then
      add_to_map(map, hexi.shape, hexi.position)
    end
  end
  if pc_hexi_shp and pc_hexi_pos then
    add_to_map(map, pc_hexi_shp, pc_hexi_pos)
  end
end

---hard drop
---@param pc_shp boolean[][]
---@param pc_pos hvec2
---@param pc_edge vec2
---@param matrix integer[][]
---@param sm_hexi_bmp (0|1|2|3)[][]
---@return boolean[][] trimmed_shape
---@return vec2 position
---@return boolean lock Whether to lock as Hiexes
local function hard_drop(pc_shp, pc_pos, pc_edge, matrix, sm_hexi_bmp)
  local pc_shp2, pc_pos2 = strip_white_lines(pc_shp, pc_pos, pc_edge)
  local pos_history = { pc_pos2 } ---@type hvec2[]
  local ret_pos_id = {} ---@type integer id to pos_history
  local cnt = 2
  pc_pos2[1] = pc_pos[1]
  pc_pos2[2] = pc_pos[2]

  -- logging contact point with SM Hexies when dropping
  while true do
    pos_history[#pos_history + 1] = pc_pos2
    if has_no_collision(pc_shp2, pc_pos2[1], pc_pos2[2] + 0.5, matrix, sm_hexi_bmp) then
      pc_pos2[2] = pc_pos2[2] + 1 -- hiexes are guaranteed to be aligned
    elseif has_no_collision(pc_shp2, pc_pos2[1] + 1, pc_pos2[2], matrix, sm_hexi_bmp) then
      pc_pos2[1] = pc_pos2[1] + 1
    elseif has_no_collision(pc_shp2, pc_pos2[1] - 1, pc_pos2[2] + 1, matrix, sm_hexi_bmp) then
      pc_pos2[2] = pc_pos2[2] + 1
      pc_pos2[1] = pc_pos2[1] - 1
    else
      break
    end
    if not has_no_collision(pc_shp2, pc_pos2[1], pc_pos2[2], matrix, sm_hexi_bmp) then
      ret_pos_id[#ret_pos_id + 1] = cnt
    end
    cnt = cnt + 1
  end

  -- TODO: try dropping again if position is invalid instead of locking it
  if has_no_collision(pc_shp2, pc_pos2[1], pc_pos2[2], matrix, sm_hexi_bmp) then
    return pc_shp2, pc_pos2, true
  end
  for i = #ret_pos_id, 1, -1 do
    if has_no_collision(pc_shp2, pos_history[i][1], pos_history[i][2], matrix, sm_hexi_bmp) then
      return pc_shp2, pos_history[i], false
    end
  end
end

---Move layers from top to bottom under layer `top` so that `bot` is the last layer to be overwritten
---@param matrix integer[][]
---@param bot integer the lowest layer to be overwritten
---@param top integer the highest layer to be moved
---@param move integer
local function move_layers_down(matrix, bot, top, move)
  local coor_top = nth_layer(top, #matrix[1])
  local coor_bot = nth_layer(bot, #matrix[1])

  for x = 1, #matrix[1] do
    for y = coor_bot[x], coor_bot[x] - move + 1, -1 do
      if y < 1 then
        break
      end

      local source = 0
      if y - move > coor_top[x] - 1 then
        source = matrix[y - move][x]
      end
      matrix[y][x] = source
    end
  end
end

---Initialize the board and other variables. Call this before calling tick
---@param board_size vec2 visual size of the matrix, including buffer zone
function setup(board_size)
  -- setting up matrix and its dimensions
  local mw = board_size[1]
  local mh = board_size[2] + math.ceil(mw / 2) - 1
  for i = 1, mh do
    g_matrix[i] = {}
    for j = 1, mw do
      g_matrix[i][j] = 0
    end
  end

  -- set up sm_hexi_map
  for i = 1, #g_matrix do
    m_sm_hexi_map[i] = {}
    for j = 1, #g_matrix[i] do
      m_sm_hexi_map[i][j] = 0
    end
  end

  -- determine g_board_[top|ceil|bot]
  g_board_top = math.ceil(mw / 2)
  g_board_bot = mh
  g_board_ceil = math.floor((g_board_top + g_board_bot) / 2)

  -- finalize g_matrix
  for i = 1, mh do
    for j = 1, mw do
      if i > nth_layer(g_board_bot, #g_matrix[1])[j] or i < nth_layer(g_board_top, #g_matrix[1])[j] then
        g_matrix[i][j] = 1
      end
    end
  end
  -- no need to care about pc hexi; the tick function will set it up
end

---@param dt number Delta time
---@param side integer The number of cell PC Hexi needs to move. positive for moving right
---@param hard boolean Whether to hard drop the PC Hexi
---@param soft_press boolean Whether the player *presses* the soft drop button
---@param rotate integer Positive for spinning clockwise
---@return TickStatus
function tick(dt, side, hard, soft_press, rotate)
  if c_collapse_wait < c_collapse_wait_cap then
    c_collapse_wait = c_collapse_wait + dt
    return TickStatus.next_hexi_wait
  end
  if c_next_hexi_wait < c_next_hexi_wait_cap then
    c_next_hexi_wait = c_next_hexi_wait + dt
    if c_next_hexi_wait >= c_next_hexi_wait_cap then
      -- generate next hexi
      local current_hexi = ListOfHexi[g_hexi_gen_bag[g_hexi_gen_progress]]
      local pos = get_pc_hexi_spawn_position(current_hexi[1], math.ceil(#g_matrix[1] / 2), g_board_ceil - 1, #g_matrix
        [1])
      if has_no_collision(current_hexi[1].shape, pos[1], pos[2], g_matrix, m_sm_hexi_map) and
          has_no_collision(current_hexi[1].shape, pos[1], pos[2] + 0.5, g_matrix, m_sm_hexi_map) then
        g_pc_hexi = current_hexi
        pos[2] = pos[2] + 0.5 -- following the design document
        g_pc_hexi_pos = pos
        g_pc_hexi_orientation = 1

        -- update gen_progress and bag
        g_hexi_gen_progress = g_hexi_gen_progress + 1
        if g_hexi_gen_progress > #g_hexi_gen_bag then
          shuf_array(g_hexi_gen_bag)
          g_hexi_gen_progress = 1
        end
      else
        -- declare game over if the hexi cannot spawn
        return TickStatus.game_over_spawn_fail
      end
    else
      return TickStatus.next_hexi_wait
    end
  end

  update_hexi_map(m_sm_hexi_map, g_sm_hexies)

  local exit_status = TickStatus.gravity_tick_wait
  local pc_pos = g_pc_hexi[g_pc_hexi_orientation].position
  local pc_shp = g_pc_hexi[g_pc_hexi_orientation].shape ---@type boolean[][]

  -- hard drop
  if hard then
    local shp2, pos2, lock = hard_drop(pc_shp,
      g_pc_hexi[g_pc_hexi_orientation].shape,
      g_pc_hexi[g_pc_hexi_orientation].position,
      g_matrix,
      m_sm_hexi_map)
    if lock then
      exit_status = TickStatus.hard_drop_lock
      lock_to_matrix(shp2, pos2[1], pos2[2], 1, g_matrix)
      f_do_collapse = true
    else
      -- stablize as sm hexi
      exit_status = TickStatus.hard_drop_sm
      local assigned = false
      for _, hexi in ipairs(g_sm_hexies) do -- reusing old entries to save space
        if not hexi.exists then
          hexi.shape = shp2
          hexi.position[1] = pos2[1]
          hexi.position[2] = pos2[2]
          hexi.exists = true
          hexi.has_moved = false
          assigned = true
        end
      end
      if not assigned then
        local newobj = { ---@type SMHexi
          shape = shp2,
          exists = true,
          has_moved = false,
          position = {}
        }
        newobj.position[1] = pos2[1]
        newobj.position[2] = pos2[2]
        g_sm_hexies[#g_sm_hexies + 1] = newobj
      end
    end
  end

  -- side movement
  if side ~= 0 and not hard then
    if side > 0 then
      side = math.min(side, #g_matrix[1] - g_pc_hexi_pos[1])
    else
      side = math.max(side, 1 - g_pc_hexi_pos)
    end
    g_pc_hexi_pos[1] = g_pc_hexi_pos[1] + side
    g_pc_hexi_pos[2] = g_pc_hexi_pos[2] - side / 2
    f_do_lock = has_no_collision(pc_shp, g_pc_hexi_pos[1], g_pc_hexi_pos[2] + 0.5, g_matrix, m_sm_hexi_map)
  end

  -- rotation
  if rotate ~= 0 and not hard then
    local new_pc_hexi_orientation = (g_pc_hexi_orientation - 1 + rotate) % 6 + 1
    local hexi_info = g_pc_hexi[new_pc_hexi_orientation]
    local new_shp = hexi_info.shape
    if has_no_collision(new_shp, g_pc_hexi_pos[1], g_pc_hexi_pos[2], g_matrix, m_sm_hexi_map) then
      g_pc_hexi_orientation = new_pc_hexi_orientation
    else
      for _, vec in ipairs(hexi_info.rotation_tests) do
        if has_no_collision(new_shp, g_pc_hexi_pos[1] + vec[1], g_pc_hexi_pos[2] + vec[2], g_matrix, m_sm_hexi_map) then
          g_pc_hexi_orientation = new_pc_hexi_orientation
          g_pc_hexi_pos[1] = g_pc_hexi_pos[1] + vec[1]
          g_pc_hexi_pos[2] = g_pc_hexi_pos[2] + vec[2]
          break
        end
      end
    end
    f_do_lock = has_no_collision(pc_shp, g_pc_hexi_pos[1], g_pc_hexi_pos[2] + 0.5, g_matrix, m_sm_hexi_map)
  end

  -- soft drop
  if soft_press and rotate == 0 and not hard then
    c_soft_drop = c_soft_drop + dt
    if c_soft_drop >= c_soft_drop_cap then
      exit_status = TickStatus.soft_drop
      f_do_lock = has_no_collision(pc_shp, g_pc_hexi_pos[1], g_pc_hexi_pos[2] + 0.5, g_matrix, m_sm_hexi_map)
      if not f_do_lock then
        g_pc_hexi_pos[2] = g_pc_hexi_pos + 0.5
      end
    end
  end

  --- gravity tick
  if f_do_lock then
    -- Set gravity timer to 0 to prevent formally-pc hexi from dropping too quickly
    c_gravity_tick = 0
  elseif c_gravity_tick < c_gravity_tick_cap then
    -- time's not up
    c_gravity_tick = c_gravity_tick + dt
  elseif not soft_press and not hard then
    c_gravity_tick = 0

    -- check whether pc hexi can fall
    if has_no_collision(pc_shp, g_pc_hexi_pos[1], g_pc_hexi_pos[2] + 0.5, g_matrix, m_sm_hexi_map) then
      -- since the pc hexi can fall, update the position of pc hexi
      g_pc_hexi_pos[2] = g_pc_hexi_pos[2] + 0.5
    end
    f_do_lock = not has_no_collision(pc_shp, g_pc_hexi_pos[1], g_pc_hexi_pos[2] + 0.5, g_matrix, m_sm_hexi_map)

    -- check f_do_lock again since the pc hexi can start locking after moving down
    if not f_do_lock then
      -- really do the gravity tick
      exit_status = TickStatus.gravity_tick

      for _, hexi in pairs(g_sm_hexies) do
        hexi.has_moved = false
      end

      -- add pc hexi into the map.
      update_hexi_map(m_sm_hexi_map, {}, pc_shp, g_pc_hexi_pos, true)

      --- moving down  -- bottom is empty
      for _, hexi in pairs(g_sm_hexies) do
        if hexi.exists then
          cut_from_map(m_sm_hexi_map, hexi.shape, hexi.position)
          if has_no_collision(hexi.shape, hexi.position[1], hexi.position[2] + 0.5, g_matrix, m_sm_hexi_map) then
            hexi.position[2] = hexi.position[2] + 0.5
            hexi.has_moved = true
          end
          add_to_map(m_sm_hexi_map, hexi.shape, hexi.position)
        end
      end
      update_hexi_map(m_sm_hexi_map, g_sm_hexies, pc_shp, g_pc_hexi_pos)

      -- moving left  -- bottom is filled,  bottom-right is filled
      for _, hexi in pairs(g_sm_hexies) do
        if hexi.exists and not hexi.has_moved then
          cut_from_map(m_sm_hexi_map, hexi.shape, hexi.position)
          if has_no_collision(hexi.shape, hexi.position[1] - 1, hexi.position[2] + 1, g_matrix, m_sm_hexi_map) and
              not has_no_collision(hexi.shape, hexi.position[1], hexi.position[2] + 0.5, g_matrix, m_sm_hexi_map) and
              not has_no_collision(hexi.shape, hexi.position[1] + 1, hexi.position[2], g_matrix, m_sm_hexi_map) then
            hexi.position[1] = hexi.position[1] + 1
            hexi.has_moved = true
          end
          add_to_map(m_sm_hexi_map, hexi.shape, hexi.position)
        end
      end
      update_hexi_map(m_sm_hexi_map, g_sm_hexies, pc_shp, g_pc_hexi_pos)

      -- moving right  -- bottom is filled
      for _, hexi in pairs(g_sm_hexies) do
        if hexi.exists and not hexi.has_moved then
          cut_from_map(m_sm_hexi_map, hexi.shape, hexi.position)
          if has_no_collision(hexi.shape, hexi.position[1] + 1, hexi.position[2], g_matrix, m_sm_hexi_map) and
              not has_no_collision(hexi.shape, hexi.position[1], hexi.position[2] + 0.5, g_matrix, m_sm_hexi_map) then
            hexi.position[1] = hexi.position[1] + 1
            -- the has_moved property doesn't matter anymore
          end
          add_to_map(m_sm_hexi_map, hexi.shape, hexi.position)
        end
      end
      update_hexi_map(m_sm_hexi_map, g_sm_hexies, pc_shp, g_pc_hexi_pos)

      -- lock
      for _, hexi in pairs(g_sm_hexies) do
        if hexi.exists then
          cut_from_map(m_sm_hexi_map, hexi.shape, hexi.position)
          if not has_no_collision(hexi.shape, hexi.position[1] - 1, hexi.position[2] + 1, g_matrix, m_sm_hexi_map) and
              not has_no_collision(hexi.shape, hexi.position[1], hexi.position[2] + 0.5, g_matrix, m_sm_hexi_map) and
              not has_no_collision(hexi.shape, hexi.position[1] + 1, hexi.position[2], g_matrix, m_sm_hexi_map) then
            lock_to_matrix(hexi.shape, hexi.position[1], hexi.position[2], 1, g_matrix)
            hexi.exists = false
          end
          add_to_map(m_sm_hexi_map, hexi.shape, hexi.position)
        end
      end
    end
  end

  -- stablizing pc hexi, mutually exclusive with gravity tick
  if f_do_lock then
    c_pc_hexi_lock = c_pc_hexi_lock + dt
    if c_pc_hexi_lock > c_pc_hexi_lock_cap then
      c_pc_hexi_lock = 0
      c_next_hexi_wait = 0
      f_do_lock = false

      local pc_hexi_face = g_pc_hexi[g_pc_hexi_orientation]

      if pc_hexi_face.lowest_position[2] + g_pc_hexi_pos[2] < nth_layer(g_board_ceil, #g_matrix[1])[pc_hexi_face.lowest_position[1]] then
        return TickStatus.game_over_over_ceiling
      end

      local trimmed_shape, adj_pos = strip_white_lines(pc_hexi_face.shape, g_pc_hexi_pos, pc_hexi_face.edge)
      local hexi_id = 0

      if has_no_collision(trimmed_shape, adj_pos[1], adj_pos[2] + 0.5, g_matrix, m_sm_hexi_map) then
        hexi_id = add_sm_hexi(trimmed_shape, adj_pos[1], adj_pos[2] + 0.5, g_sm_hexies)
      elseif has_no_collision(trimmed_shape, adj_pos[1] + 1, adj_pos[2], g_matrix, m_sm_hexi_map) then
        hexi_id = add_sm_hexi(trimmed_shape, adj_pos[1] + 1, adj_pos[2], g_sm_hexies)
      elseif has_no_collision(trimmed_shape, adj_pos[1] - 1, adj_pos[2] + 1, g_matrix, m_sm_hexi_map) then
        hexi_id = add_sm_hexi(trimmed_shape, adj_pos[1] - 1, adj_pos[2] + 1, g_sm_hexies)
      else
        f_do_collapse = true
        lock_to_matrix(trimmed_shape, adj_pos[1], adj_pos[2], 1, g_matrix)
      end

      if hexi_id == 0 then
        -- no need to cut shape from map -- the don't exist on the map yet
        if not has_no_collision(trimmed_shape, adj_pos[1] - 1, adj_pos[2] + 1, g_matrix, m_sm_hexi_map) and
            not has_no_collision(trimmed_shape, adj_pos[1], adj_pos[2] + 0.5, g_matrix, m_sm_hexi_map) and
            not has_no_collision(trimmed_shape, adj_pos[1] + 1, adj_pos[2], g_matrix, m_sm_hexi_map) then
          lock_to_matrix(trimmed_shape, adj_pos[1], adj_pos[2], 1, g_matrix)
          g_sm_hexies[hexi_id].exists = false
        end
      end
      -- TODO Add a flag to signify that the PC Hexi no longer exists (to prevent visual glitches)
    end
  else -- if f_do_lock
    c_pc_hexi_lock = 0
  end

  if f_do_collapse then
    f_do_collapse = false
    if exit_status ~= TickStatus.hard_drop_lock then
      exit_status = TickStatus.collapse
    end
    -- collapsing
    local c_full_layer = 0
    for i = g_board_top, g_board_bot do
      local f_layer_full = true
      for x, y in ipairs(nth_layer(i, #g_matrix[1])) do
        if g_matrix[y][x] == 0 then
          f_layer_full = false
          break
        end
      end
      if f_layer_full then
        c_full_layer = c_full_layer + 1
      else
        move_layers_down(g_matrix, i - 1, g_board_top, c_full_layer)
      end
    end
  end

  return exit_status
end

function get_matrix()
  return g_matrix
end

---@return SMHexi[]
---@return {shape: boolean[][], position: hvec2}
function get_hexies()
  return g_sm_hexies, { shape = g_pc_hexi[g_pc_hexi_orientation].shape, position = g_pc_hexi_pos }
end

---@return integer[] top
---@return integer[] ceil
---@return integer[] bot
function get_matrix_info()
  return nth_layer(g_board_top, #g_matrix[1]), nth_layer(g_board_ceil, #g_matrix[1]),
      nth_layer(g_board_bot, #g_matrix[1])
end
