雜事
====

- 把所有的shp參數改名為bmp
- 把整個game.lua打包成class

修理（簡單）
===========

- 加個函式把g_matrix與map合在一起，以減少has_no_collision的參數量
- 在PC Hexi鎖定之後，如果PC Hexi鎖定成SM Hexi，自主移動一次

修理（複雜）
===========

## 1. SM Hexi下落時無法上下緊貼

SM Hexi的下落順序可以用拓撲排序來決定，但是圖可能有迴圈，所以點在加入圖時要看有沒有形成迴圈。
