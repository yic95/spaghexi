---@class AutoRepeatAxie
---@field auto_repeat_interval number
---@field auto_repeat_interval_initial number
---@field timer number
---@field previous_input_status {[1]: boolean, [2]: boolean}
---@field is_initial_repeat boolean
---@field repeating_key 0|1|2
AutoRepeatAxie = {}

---@param delay number
---@param initial_delay number
---@return AutoRepeatAxie
function AutoRepeatAxie:new(delay, initial_delay)
  local obj = {
    auto_repeat_interval = delay,
    auto_repeat_interval_initial = initial_delay,
    timer = 0,
    previous_input_status = {false, false},
    is_initial_repeat = false,
    repeating_key = 0
  }

  setmetatable(obj, {__index=AutoRepeatAxie})
  return obj
end

---@param dt number
---@param is_button1_down boolean
---@param is_button2_down boolean
---@return 1|2|false
function AutoRepeatAxie:tick(dt, is_button1_down, is_button2_down)
  self.timer = self.timer + dt

  local result = false  ---@type 1|2|false
  -- Key first pressed
  if not self.previous_input_status[1] and is_button1_down then
    self.is_initial_repeat = true
    self.timer = 0
    self.repeating_key = 1
    result = 1
  elseif not self.previous_input_status[2] and is_button2_down then
    self.is_initial_repeat = true
    self.timer = 0
    self.repeating_key = 2
    result = 2
  end

  local is_timer_overflow = self.timer > (self.is_initial_repeat and self.auto_repeat_interval_initial or self.auto_repeat_interval)
  if is_timer_overflow then
    self.timer = 0
  end

  if is_timer_overflow and is_button1_down and self.repeating_key == 1 then
    self.is_initial_repeat = false
    result = 1
  elseif is_timer_overflow and is_button2_down and self.repeating_key == 2 then
    self.is_initial_repeat = false
    result = 2
  end

  if not is_button1_down and self.repeating_key == 1 then
    self.repeating_key = 0
  end
  if not is_button2_down and self.repeating_key == 2 then
    self.repeating_key = 0
  end

  self.previous_input_status = {is_button1_down, is_button2_down}

  return result
end

function AutoRepeatAxie:reset()
  self.previous_input_status = {false, false}
  self.is_initial_repeat = true
  self.timer = 0
  self.repeating_key = 0
end

---@class RisingEdgeButton
---@field previous_input_status boolean
RisingEdgeButton = {}

---Initializer
---@return RisingEdgeButton
function RisingEdgeButton:new()
  local obj = {
    previous_input_status = false
  }
  setmetatable(obj, {__index=RisingEdgeButton})
  return obj
end

function RisingEdgeButton:tick(is_button_down)
  local result = false
  if is_button_down and self.previous_input_status == false then
    result = true
  end
  self.previous_input_status = is_button_down

  return result
end

function RisingEdgeButton:reset()
  self.previous_input_status = false
end

